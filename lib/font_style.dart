import 'package:flutter/material.dart';
import 'package:pertama/custom_color.dart';

TextStyle mainheader = TextStyle(
    fontSize: 26,
    color: gold,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w700);

TextStyle subheader = TextStyle(
    fontSize: 14,
    color: Colors.black,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w300);
