import 'package:flutter/material.dart';
import 'package:pertama/custom_color.dart';
import 'package:pertama/font_style.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
            appBar: AppBar(
              backgroundColor: gold,
              title: Text("Cashy"),
            ),
            body: SafeArea(
                child: Container(
                    margin:
                        EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
                    padding:
                        EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image(
                              image: AssetImage('assets/images/icon.png'),
                              height: 200,
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20, bottom: 4),
                              child: Text(
                                "Rich Together",
                                style: mainheader,
                              ),
                            ),
                            Text(
                              "Sedikit demi sedikit lama-lama jadi kaya",
                              style: subheader,
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ],
                    )))));
  }
}
